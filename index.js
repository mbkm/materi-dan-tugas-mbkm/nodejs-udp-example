const udp = require('dgram');;
const server = udp.createSocket('udp4');

const config = {
	ip: 'localhost',
	port: 12345,
};

server.on('error',function(error){
	console.error('Error: ', error);
	server.close();
});

server.on('listening',function(){
	const address = server.address();
	const port = address.port;
	const family = address.family;
	const ipaddr = address.address;

	console.log('Server is listening at port', port);
	console.log('Server ip :', ipaddr);
	console.log('Server is IP4/IP6 : ', family);
});

server.on('close',function(){
	console.log('Socket is closed !');
});

server.on('message', function (message, info) {
	const sentData = (() => {
		try{
			const obj = JSON.parse(message.toString());

			if(!obj.request){
				throw 'invalid';
			}

			return {
				timestamp: Date.now(),
				data: parseInt(Math.random() * 10000),
			};
		} catch (error) {
			return error === 'invalid'
				? 'message should be in form of {"request": true}'
				: 'message should be a JSON';
		}
	})();

	server.send(JSON.stringify(sentData) + '\n', info.port, info.address , error => {
		if(error){
			console.error(error);
			return;
		}
	});
});

server.bind(config.port, config.ip);
